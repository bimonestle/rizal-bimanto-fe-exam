import { Component, OnInit } from '@angular/core';
import { APIserviceService } from '../APIservice/apiservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList : object[];

  constructor(private data:APIserviceService, private router: Router) { }

  ngOnInit() {
    this.userList = this.data.getUserList();
  }

  removeUser(id)
  {
    this.userList = this.data.removeUserList(id);
  }

}
