import { Component, OnInit } from '@angular/core';
import { APIserviceService } from '../APIservice/apiservice.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  Name: string = '';
  Email: string;
  Address: string;
  PhoneNumber: string;
  CompanyName: string;

  constructor(private data:APIserviceService) { }

  ngOnInit() {
  }

  onSubmit(myForm: NgForm)
  {
    console.log(myForm.value);
    console.log(myForm.valid);
  }

  addUser()
  {
    this.data.addUserList
    (
      {
        "Name":this.Name,
        "Email":this.Email,
        "Address":this.Address,
        "PhoneNumber":this.PhoneNumber,
        "CompanyName":this.CompanyName
      }
    );
  }

}
