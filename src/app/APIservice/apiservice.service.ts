import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class APIserviceService {

  private userList: Object[] = 
  [{
      "ID":"",
      "Name": "",
      "Email": "",
      "Address": "",
      "PhoneNumber": "",
      "CompanyName": "",
    }]

  constructor(private http:Http) { }

  getUserList()
  {
    return this.http
    .get('https://jsonplaceholder.typicode.com/users');
  }

  getUser(ID:string):object
  {
    var obj:object;
    this.userList.forEach(user => {
      if (user['ID'] == ID) {
        obj = user;
      }
    });
    return obj;
  }

addUserList(obj:object):object[]
  {
    this.userList.push(obj);
    return this.userList;
  }

  removeUserList(ID:string):object[]
  {
    for (var i = 0; i < this.userList.length; i++)
    {
      if (this.userList[i]["ID"] == ID)
      {
        this.userList.splice(i, 1);
        break;
      }
    }
    return this.userList;
  }
} 
