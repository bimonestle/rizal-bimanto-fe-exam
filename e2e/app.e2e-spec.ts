import { UjianFEPage } from './app.po';

describe('ujian-fe App', () => {
  let page: UjianFEPage;

  beforeEach(() => {
    page = new UjianFEPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
